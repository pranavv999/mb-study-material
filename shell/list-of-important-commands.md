# Important Linux commands
The command that are relevant to us from the article https://www.thegeekstuff.com/2010/11/50-linux-commands/

The commands are sorted by "most commonly used", so make sure you pay more importance to the top commands in each section.

### Help
1. man

### File system operations
1. ls
2. cd
3. rm
4. cp
5. mv
6. mkdir
7. pwd
8. find
9. whereis
    

### Shell utilities
1. export
1. sort
2. xargs
3. date

### File content 
1. less
2. grep
3. cat
4. diff
5. tail
6. awk
7. sed

### Processes
1. ps
2. kill

### File permissions
1. chmod
2. chown
   
### Resource utilization (memory, hard disk, etc.)
1. free
2. top
3. df

### Networking, remote access
1. ping
2. wget
3. ftp (needs ftp server to try out)
4. ssh (needs ssh server to try out)

### Archiving and Compression
1. unizp
2. tar
3. gzip
4. bzip2

### System commands
1. su
2. password
3. service
4. shutdown
5. mount
6. uname
7. crontab


