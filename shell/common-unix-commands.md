
## Searching



## Archiving and Compressing Files


### the `tar` command

Create a new tar archive.

```shell
tar cvf archive_name.tar dirname/
```

Extract from an existing tar archive.

```shell
tar xvf archive_name.tar
```

View an existing tar archive.

```shell
tar tvf archive_name.tar
```


